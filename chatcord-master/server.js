const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const socketio = require('socket.io');
const assert = require('assert');

const DB = 'mongodb+srv://mern:mern@cluster0.zjxdf.mongodb.net/mern?retryWrites=true&w=majority';
mongoose.connect(DB, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false
}).then(() => {
  console.log(`we are connected`)
}).catch((err) => console.log(`failed to conneted`));

const userSchema = new mongoose.Schema({
  Name: {
    type: String,
    required: true
  },
  Email: {
    type: String,
    required: true
  },
  Phone: {
    type: String,
    required: true
  },
  Description: {
    type: String,
    required: true
  }
})
const User = new mongoose.model("User", userSchema);

// const postUserData = new User({

// })
const formatMessage = require('./utils/messages');
const {
  userJoin,
  getCurrentUser,
  userLeave,
  getRoomUsers
} = require('./utils/users');
const { url } = require('inspector');

const app = express();
const server = http.createServer(app);
const io = socketio(server);
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// Set static folder
app.use(express.static(path.join(__dirname, 'public')));


app.post('/insert', function (req, res, next) {
  const users = new User(req.body);
  users.save(function (err, users) {
    if (err) {
      console.log('posted')
      res.send('posted');
    }
    else {
      console.log('failed posted')
      res.send('failed posted');
    }
  })
});
app.get("/insert", (req, res) => {
  User.find({}, (err, result) => {
      if(err) {
          res.status(200).json({error: err});
      } else {
          // res.sendFile(path.resolve('./src/public/index.html'));
          res.send(result);  
      } 
  });
});

const botName = 'ChatCord Bot';

// Run when client connects
io.on('connection', socket => {
  socket.on('joinRoom', ({ username, room }) => {
    const user = userJoin(socket.id, username, room);

    socket.join(user.room);

    // Welcome current user
    socket.emit('message', formatMessage(botName, 'Welcome to ChatCord!'));

    // Broadcast when a user connects
    socket.broadcast
      .to(user.room)
      .emit(
        'message',
        formatMessage(botName, `${user.username} has joined the chat`)
      );

    // Send users and room info
    io.to(user.room).emit('roomUsers', {
      room: user.room,
      users: getRoomUsers(user.room)
    });
  });

  // Listen for chatMessage
  socket.on('chatMessage', msg => {
    const user = getCurrentUser(socket.id);

    io.to(user.room).emit('message', formatMessage(user.username, msg));
  });

  // Runs when client disconnects
  socket.on('disconnect', () => {
    const user = userLeave(socket.id);

    if (user) {
      io.to(user.room).emit(
        'message',
        formatMessage(botName, `${user.username} has left the chat`)
      );

      // Send users and room info
      io.to(user.room).emit('roomUsers', {
        room: user.room,
        users: getRoomUsers(user.room)
      });
    }
  });
});

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => console.log(`Server running on port ${PORT}`));
